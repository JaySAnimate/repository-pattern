﻿namespace RepositoryPattern
{
    interface IPCPartsTableRepository : IRepository<Table>
    { }
}