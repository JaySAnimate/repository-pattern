﻿namespace RepositoryPattern
{
    interface IPCPartsStoreRepository : IRepository<Store>
    {
        Store GetByAddress(string address);
    }
}