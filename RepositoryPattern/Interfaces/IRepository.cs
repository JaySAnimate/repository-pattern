﻿using System.Collections.Generic;

namespace RepositoryPattern
{
    interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Insert(T item);
        void Delete(int id);
    }
}
