﻿namespace RepositoryPattern
{
    interface IUnitOfWork
    {
        IPCPartsStoreRepository StoreEntity { get;}
        IPCPartsTableRepository TableEntity { get;}
    }
}