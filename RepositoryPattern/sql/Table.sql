USE [JaysPC]
GO

/****** Object:  Table [dbo].[Table]    Script Date: 02.12.2019 04:25:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Table](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Vendor] [nvarchar](50) NOT NULL,
	[Category] [nvarchar](50) NOT NULL,
	[Model] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Present] [bit] NOT NULL,
	[Storage] [int] NULL,
	[Unit] [nvarchar](50) NULL,
	[Frequency] [float] NULL,
	[FrequencyUnit] [nvarchar](50) NULL,
	[Desription] [text] NULL,
	[PackageBarcode] [nvarchar](50) NOT NULL,
	[ItemBarcode] [nvarchar](50) NOT NULL,
	[InStoreMin] [int] NOT NULL,
	[InwarehouseMin] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Table] ADD  DEFAULT ((0)) FOR [InStoreMin]
GO

ALTER TABLE [dbo].[Table] ADD  DEFAULT ((0)) FOR [InwarehouseMin]
GO

