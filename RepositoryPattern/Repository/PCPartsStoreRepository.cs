﻿using System.Collections.Generic;
using System.Linq;

namespace RepositoryPattern
{
    class PCPartsStoreRepository : IPCPartsStoreRepository
    {
        protected readonly JaysPCEntities _context;

        public PCPartsStoreRepository(ref JaysPCEntities context) => _context = context;
        public void Delete(int id) => _context.Store.Remove(_context.Store.Find(id));
        public IEnumerable<Store> GetAll() => _context.Store.ToList();
        public Store GetById(int id) => _context.Store.Find(id);
        public Store GetByAddress(string address) => _context.Store.Where(item => item.Adress == address).FirstOrDefault();
        public void Insert(Store item) => _context.Store.Add(item);
    }
}