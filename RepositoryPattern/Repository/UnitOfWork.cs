﻿namespace RepositoryPattern
{
    class UnitOfWork : IUnitOfWork
    {
        protected readonly JaysPCEntities _context;
        public IPCPartsStoreRepository StoreEntity { get; private set; }
        public IPCPartsTableRepository TableEntity { get; private set; }
        public UnitOfWork(JaysPCEntities context)
        {
            _context = context;
            StoreEntity = new PCPartsStoreRepository(ref _context);
            TableEntity = new PCPartsTableRepository(ref _context);
        }
        public int Save() => _context.SaveChanges();
    }
}
