﻿using System.Collections.Generic;
using System.Linq;

namespace RepositoryPattern
{
    class PCPartsTableRepository : IPCPartsTableRepository
    {
        protected readonly JaysPCEntities _context;

        public PCPartsTableRepository(ref JaysPCEntities context) => _context = context;
        public void Delete(int id) => _context.Table.Remove(_context.Table.Find(id));
        public IEnumerable<Table> GetAll() => _context.Table.ToList();
        public Table GetById(int id) => (Table)_context.Table.Where(item => item.Id == id);
        public void Insert(Table item) => _context.Table.Add(item);
    }
}