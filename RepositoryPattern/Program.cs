﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace RepositoryPattern
{
    class Program
    {
        static void Main()
        {
            try
            {
                JaysPCEntities Context = new JaysPCEntities();
                UnitOfWork Work = new UnitOfWork(Context);
                IEnumerable<Table> PCPartsTable = Work.TableEntity.GetAll();
                IEnumerable<Store> PCPartsStore = Work.StoreEntity.GetAll();

                Console.WriteLine("-------------------------Models From Table-------------------------");
                foreach (var i in PCPartsTable)
                    Console.WriteLine("Model #{0}: {1}", i.Id, i.Model);
                Console.WriteLine();

                Console.WriteLine("-------------------------------Stores------------------------------");
                foreach (var i in PCPartsStore)
                    Console.WriteLine("Store #{0}: {1} {2}", i.Id, i.City, i.Adress);
                Console.WriteLine();

                Store NewStore = new Store()
                {
                    Adress = "G9845 Posting",
                    State = "AR",
                    City = "Ilona",
                };

                Work.StoreEntity.Insert(NewStore);
                Work.Save();
                Store Inserted = Work.StoreEntity.GetByAddress(NewStore.Adress);
                Console.WriteLine("New Store Inserted is: {0}", Inserted.Adress + Inserted.City);

                Console.WriteLine("---------------------------------------------------------------------");

                Store store = Work.StoreEntity.GetById(2);
                Console.WriteLine($"Store Address to be deleted...\n{store.Adress} {store.City}");
                Work.StoreEntity.Delete(store.Id);
                Thread.Sleep(1500);
                Work.Save();
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                Console.WriteLine(e.Message);
            }
            catch(ArgumentException ane)
            {
                Console.WriteLine(ane.Message);
            }
        }
    }
}
